plugins {
    id("org.jetbrains.kotlin.jvm").version("1.3.10")
    application
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation(project(":di"))
}

application {
    mainClassName = "de.earley.adventOfCode.week2.examples.AppKt"
}
