package de.earley.adventOfCode.week2.examples.coffeeShop

import de.earley.adventOfCode.week2.Context

fun coffeeShop() {
	println("CoffeeShop")
	val inject = Context<DI>(CoffeeShop())
	inject { coffeeMaker }.brew()

	val testInject = Context<DI>(CoffeeShop(TestDripCoffeeModule()))
	testInject { coffeeMaker }.brew()
}