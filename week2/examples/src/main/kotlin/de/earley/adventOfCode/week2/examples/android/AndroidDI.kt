package de.earley.adventOfCode.week2.examples.android

import de.earley.adventOfCode.week2.Context
import de.earley.adventOfCode.week2.Provider
import de.earley.adventOfCode.week2.singleton

/**
 * Only the [PresenterDI] instances can be directly injected.
 * The view cannot inject things from [ModelDI].
 */
interface DI : PresenterDI

/**
 * The model has no dependencies.
 */
interface ModelDI {
	val model: Provider<Model>
}

class ModelDIImpl : ModelDI {
	override val model: Provider<Model> = singleton { Model() }
}

interface PresenterDI {
	val presenter: Provider<Presenter>
}

/**
 * The presenter depends on the model
 */
class PresenterDIImpl(
	modelDI: ModelDI
) : PresenterDI {
	override val presenter: Provider<Presenter> = { get -> Presenter(get(modelDI.model)) }
}

/**
 * Defaults can be overridden for testing, e.g. the model swapped for a mocked model.
 */
class DIImpl(
	modelDI: ModelDI = ModelDIImpl(),
	presenter: PresenterDI = PresenterDIImpl(modelDI)
) : DI, PresenterDI by presenter


lateinit var inject: Context<DI>