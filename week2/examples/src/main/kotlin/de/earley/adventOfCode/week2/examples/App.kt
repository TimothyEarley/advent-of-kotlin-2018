package de.earley.adventOfCode.week2.examples

import de.earley.adventOfCode.week2.examples.android.android
import de.earley.adventOfCode.week2.examples.coffeeShop.coffeeShop

fun main(args: Array<String>) {
	coffeeShop()
	println()
	android()
}
