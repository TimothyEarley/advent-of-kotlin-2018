package de.earley.adventOfCode.week2.examples.coffeeShop

import de.earley.adventOfCode.week2.Provider

interface DI {
	val coffeeMaker: Provider<CoffeeMaker>
}

open class DripCoffeeModule {
    open val heater: Provider<Heater> = { ElectricHeater() }
    open val pump: Provider<Pump> = { get -> Thermosiphon(get(heater)) }
}

class CoffeeShop(
	dripCoffeeModule: DripCoffeeModule = DripCoffeeModule()
): DI {
    override val coffeeMaker: Provider<CoffeeMaker> = { get ->
            CoffeeMaker(lazy { get(dripCoffeeModule.heater) }, get(dripCoffeeModule.pump))
    }
}
