package de.earley.adventOfCode.week2.examples.coffeeShop

interface Heater {
    fun on()
    fun off()
}

class ElectricHeater : Heater {
    override fun on() {
        println("~ ~ ~ heating on ~ ~ ~")
    }
    
    override fun off() {
        println("~ ~ ~ heating off ~ ~ ~")
    }
}

interface Pump {
    fun pump()
}

class Thermosiphon(private val heater: Heater) : Pump {
    override fun pump() {
        heater.on()
        println(". . . pumping . . .")
        heater.off()
    }
}

class CoffeeMaker(
    lazyHeater: Lazy<Heater>,
    private val pump: Pump
) {
    
    private val heater by lazyHeater
    
    fun brew() {
        heater.on()
        pump.pump()
        println(" [_]P coffee! [_]P ")
        heater.off()
    }
    
}