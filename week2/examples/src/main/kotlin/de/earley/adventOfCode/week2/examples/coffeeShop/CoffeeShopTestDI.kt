package de.earley.adventOfCode.week2.examples.coffeeShop

import de.earley.adventOfCode.week2.Provider
import de.earley.adventOfCode.week2.singleton


class TestDripCoffeeModule : DripCoffeeModule() {
	override val heater: Provider<Heater> = singleton { object : Heater {
		override fun on() {
			println("Test on")
		}

		override fun off() {
			println("Test off")
		}

	}}

	override val pump: Provider<Pump> = singleton { object : Pump {
		override fun pump() {
			println("Test pump")
		}
	}}
}