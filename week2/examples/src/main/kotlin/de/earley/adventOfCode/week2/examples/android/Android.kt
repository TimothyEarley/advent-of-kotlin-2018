package de.earley.adventOfCode.week2.examples.android

class Model {
	fun doStuff(): String = "Result from model"
}

class View {

	private val presenter = inject { presenter }

	fun onClick() {
		presenter.initiateStuff(this)
	}

	fun showResult(result: String) {
		println("View: $result")
	}

}

//TODO: use a contract interface

class Presenter(
	private val model: Model
) {

	fun initiateStuff(view: View) {
		val result = model.doStuff()
		view.showResult(result)
	}

}
