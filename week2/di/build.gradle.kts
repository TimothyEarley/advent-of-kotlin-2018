plugins {
    id("org.jetbrains.kotlin.jvm").version("1.3.10")
}


dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
}
