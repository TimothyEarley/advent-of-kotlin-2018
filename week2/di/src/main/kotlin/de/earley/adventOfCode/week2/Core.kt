package de.earley.adventOfCode.week2

/**
 * A provider creates a (not necessarily new) instance of T.
 * It may use the given injector to get further dependencies.
 */
typealias Provider<T> = (Injector) -> T

/**
 * The injector is the way to invoke a provider.
 * The reason it exists is to allow a controller for invoking providers. This controller
 * could for example be used for overriding (mocking) providers.
 * It is also used in [singleton].
 */
class Injector {
	operator fun <T> invoke(provider: Provider<T>): T = provider(this)
}

/**
 * The main entry point for using the library.
 * The [di] variable holds a reference to each available [Provider].
 * To get the correct provider, an extension function is used in [invoke].
 *
 * This means the user has to declare an interface ([DI]) with all available [Provider] instances
 * beforehand. This extra verbosity guarantees that the instance is always found at runtime.
 *
 * The call site is not much different from conventional class based injection.
 *
 * Compare:
 * val foo = inject<Foo>()
 * vs.
 * val foo = inject { foo }
 *
 */
class Context<DI>(private val di: DI) {

	// unique instance per Context
	private val inject = Injector()

	operator fun <T> invoke(getProvider: DI.() -> Provider<T>): T = inject(di.getProvider())

}