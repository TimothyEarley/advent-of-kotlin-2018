package de.earley.adventOfCode.week2

import java.util.*

/**
 * Confines the provider to one instance per [Injector].
 */
fun <T> singleton(provider: Provider<T>): Provider<T> {
	/*
	 The cache is both thread-safe (thanks to [synchronizedMap]) and also does not hold onto
	 instances fot longer than necessary (thanks to [WeakHashMap]).

	 Thus this method can safely be used across multiple threads and injectors.
	 */
	val cache = Collections.synchronizedMap(WeakHashMap<Injector, T>())

	return { get ->
		cache.getOrPut(get) {
			provider(get)
		}
	}
}