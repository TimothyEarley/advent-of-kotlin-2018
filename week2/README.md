Advent of Kotlin Week 2
=======================

This is a reduced version of [KompanionDI](https://github.com/TimothyEarley/kompanionDI) (WIP) for the [Advent of Kotlin Week 2](https://blog.kotlin-academy.com/advent-of-kotlin-week-2-dependency-injection-351f615da2e1).

Goals:
- fast (no reflection)
- safe (no runtime errors)
- uninvasive (most of the code does not need to know about DI, just use constructors)

The important file (with code and more information) is in [`di/src/main/kotlin/de/earley/adventOfCode/week2/Core.kt`](week2/di/src/main/kotlin/de/earley/adventOfCode/week2/Core.kt).

Comparing it with the constructs mentioned on the instructions for the challenge we have:
- Inject: both have an inject method, but the example takes a class, mine takes an instance (in an indirect manner)
    - Instances means no special code needed to inject different variants of the same type
- Registry: the example uses a Map<String, ...>; I use a class (which essentially is a safe Map if you think of the variable names as the key)
- DSL: The example uses a DSL to create instances; I use class properties 
    - DSL is shorter (only declared once vs. interface and implementation)
    - Class properties are safe (existence is checked at compile time)